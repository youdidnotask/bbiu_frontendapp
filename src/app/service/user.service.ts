import { Injectable } from '@angular/core';

const KEY_USER_ID = 'user-id'
const KEY_USER_EMAIL = 'user-email'
const KEY_USER_SCOPE = 'user-scope';
const KEY_USER_TOKEN = 'user-token';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor() { }

  public signIn(userData) {
    window.sessionStorage.clear();
    window.sessionStorage.setItem(KEY_USER_ID, userData.id);
    window.sessionStorage.setItem(KEY_USER_EMAIL, userData.username);
    window.sessionStorage.setItem(KEY_USER_SCOPE, userData.scope);
    window.sessionStorage.setItem(KEY_USER_TOKEN, userData.token);
  }

  public signOut() {
    window.sessionStorage.clear();
  }

  public getId() {
    return window.sessionStorage.getItem(KEY_USER_ID);
  }

  public getEmail() {
    return window.sessionStorage.getItem(KEY_USER_EMAIL);
  }

  public getToken() {
    return window.sessionStorage.getItem(KEY_USER_TOKEN);
  }

  public isAuthenticated() {
    return window.sessionStorage.getItem(KEY_USER_TOKEN) !== null;
  }

  public hasRole(role) {
    const scope = window.sessionStorage.getItem(KEY_USER_SCOPE);
    return scope && scope.includes(role);
  }
}
