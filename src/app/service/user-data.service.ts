import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from './user.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type' : 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  constructor(
    private userService: UserService,
    private httpClient: HttpClient) { }

  getUserDetails(userId): Observable<any> {
    return this.httpClient.get(
      environment.server_url + '/users/account?userId=' + userId,
      httpOptions
    );
  }

  getEmployees(): Observable<any> {
    return this.httpClient.get(
      environment.server_url + '/users/employees',
      httpOptions
    );
  }

  assignActivity(data): Observable<any> {
    return this.httpClient.post(
      environment.server_url + '/users/employees/activity',
      data,
      httpOptions
    );
  }
}
