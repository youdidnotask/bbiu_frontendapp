import { Injectable } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from './user.service';

const HEADER_AUTHORIZATION = 'Authorization';
const AUTHORIZATION_TYPE = 'Bearer ';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private userService: UserService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authRequest;
    const token = this.userService.getToken();
    if (token != null) {
      authRequest = req.clone({
        headers: req.headers.set(HEADER_AUTHORIZATION, AUTHORIZATION_TYPE + ' ' + token)
      });
    } else {
      authRequest = req;
    }
    return next.handle(authRequest);
  }
}

export const authInterceptorProviders = [{
  provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true
}];
