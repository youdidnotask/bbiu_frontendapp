import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type' : 'application/json'})
}


@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getActivities(): Observable<any> {
    return this.httpClient.get(
      environment.server_url + '/schedule/activities',
      httpOptions
    );
  }

  getRooms(): Observable<any> {
    return this.httpClient.get(
      environment.server_url + '/schedule/rooms',
      httpOptions
    );
  }

  get(): Observable<any> {
    return this.httpClient.get(
      environment.server_url + '/schedule',
      httpOptions
    );
  }

  save(data): Observable<any> {
    return this.httpClient.post(
      environment.server_url + '/schedule',
      data,
      httpOptions
    );
  }

  signIn(scheduleId): Observable<any> {
    return this.httpClient.get(
      environment.server_url + '/schedule/signin',
      {
        headers: new HttpHeaders({'Content-Type' : 'application/json'}),
        params: {scheduleId}
      }
    );
  }

  signOut(scheduleId): Observable<any> {
    return this.httpClient.get(
      environment.server_url + '/schedule/signout',
      {
        headers: new HttpHeaders({'Content-Type' : 'application/json'}),
        params: {scheduleId}
      }
    );
  }
}
