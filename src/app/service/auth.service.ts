import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { UserService } from './user.service';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type' : 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private userService: UserService,
    private httpClient: HttpClient) { }

  registerEmployee(employee): Observable<any> {
    return this.httpClient.post(
      environment.server_url + '/security/register-employee',
      {
        username: employee.email,
        password: employee.password
      },
      httpOptions
    );
  }

  register(user): Observable<any> {
    return this.httpClient.post(
      environment.server_url + '/security/signup',
      {
        username: user.email,
        password: user.password
      },
      httpOptions
    );
  }

  login(credentials): Observable<any> {
    return this.httpClient.post(
      environment.server_url + '/security/generate-token',
      {
        username: credentials.email,
        password: credentials.password
      },
      httpOptions
    );
  }
}
