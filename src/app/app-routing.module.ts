import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './components/registration/registration.component';
import {LoginComponent} from './components/login/login.component';
import {RegistrationEmployeeComponent} from './components/registration-employee/registration-employee.component';
import {EmployeesComponent} from './components/employees/employees.component';
import {ProfileComponent} from './components/profile/profile.component';
import {ScheduleComponent} from './components/schedule/schedule.component';
import {ActivityComponent} from './components/activity/activity.component';


const routes: Routes = [
  { path: 'register',         component: RegistrationComponent },
  { path: 'login',            component: LoginComponent },
  { path: 'registeremployee', component: RegistrationEmployeeComponent },
  { path: 'employees',        component: EmployeesComponent },
  { path: 'profile',          component: ProfileComponent },
  { path: 'activity',         component: ActivityComponent },
  { path: 'schedule',         component: ScheduleComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
