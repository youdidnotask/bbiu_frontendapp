import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../../service/user-data.service';
import {UserService} from '../../service/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  employees;

  constructor(
    private userService: UserService,
    private userDataService: UserDataService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (!this.userService.hasRole('ROLE_ADMIN')) {
      this.router.navigateByUrl('/');
    }
    this.userDataService.getEmployees().subscribe(
      data => {
        this.employees = data;
      }
    );
  }

}
