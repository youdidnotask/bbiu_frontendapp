import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../service/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm;
  errorMsg;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  onSubmit(credentials): void {
    this.errorMsg = '';
    this.authService.login(credentials).subscribe(
      data => {
        this.userService.signIn(data);
        this.router.navigateByUrl('/');
      },
      response => {
        this.errorMsg = response.error;
      }
    );
  }

  get email()     { return this.loginForm.get('email'); }
  get password()  { return this.loginForm.get('password'); }
}
