import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration-employee',
  templateUrl: './registration-employee.component.html',
  styleUrls: ['./registration-employee.component.css']
})
export class RegistrationEmployeeComponent implements OnInit {
  registrationForm;
  errorMsg;
  successMsg;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.registrationForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  ngOnInit(): void {
    if (!this.userService.hasRole('ROLE_ADMIN')) {
      this.router.navigateByUrl('/');
    }
  }

  onSubmit(employeeData): void {
    this.errorMsg = '';
    this.successMsg = '';
    this.authService.registerEmployee(employeeData).subscribe(
      data => {
        this.successMsg = 'Employee successfully registered';
        this.registrationForm.reset();
      },
      response => {
        this.errorMsg = response.error;
      }
    );
  }

  get email()     { return this.registrationForm.get('email'); }
  get password()  { return this.registrationForm.get('password'); }
}
