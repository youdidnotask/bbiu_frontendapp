import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../service/auth.service';
import { Match } from '../../validation/match.validator';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registrationForm;
  errorMsg;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.registrationForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]],
      password2: ['', Validators.required]
    }, { validator: Match('password', 'password2')});
  }

  ngOnInit(): void {
  }

  onSubmit(userData): void {
    this.errorMsg = '';
    this.authService.register(userData).subscribe(
      data => {
        this.router.navigateByUrl('/login');
      },
      response => {
        this.errorMsg = response.error;
        this.registrationForm.reset();
      }
    );
  }

  get email()     { return this.registrationForm.get('email'); }
  get password()  { return this.registrationForm.get('password'); }
  get password2() { return this.registrationForm.get('password2'); }
}
