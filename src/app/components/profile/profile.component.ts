import {Component, OnInit} from '@angular/core';
import {UserDataService} from '../../service/user-data.service';
import {UserService} from '../../service/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ScheduleService} from '../../service/schedule.service';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userId;
  userData;
  activityList;
  activityForm;

  constructor(
    private userService: UserService,
    private userDataService: UserDataService,
    private scheduleService: ScheduleService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.activityForm = this.formBuilder.group({
      activityId: ['', Validators.required]
    });
    this.route.queryParams.subscribe(
      params => {
        this.userId = params.userId;
      }
    );
  }

  ngOnInit(): void {
    if (!this.userService.isAuthenticated()) {
      this.router.navigateByUrl('/');
    }
    this.userDataService.getUserDetails(this.userId).subscribe(
      data => {
        this.userData = data;
      }
    );
    this.scheduleService.getActivities().subscribe(
      data => {
        this.activityList = data;

      }
    );
  }

  saveActivity(activityData): void {
    this.userDataService.assignActivity({
      userId: this.userId,
      activityId: activityData.activityId
    }).subscribe(
      data => {
        this.userData.activities = data;
      }
    );
  }

}
