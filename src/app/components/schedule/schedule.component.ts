import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ScheduleService} from '../../service/schedule.service';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {
  scheduleList;
  mySchedule;

  constructor(
    public userService: UserService,
    private scheduleService: ScheduleService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.scheduleService.get().subscribe(
      data => {
        this.scheduleList = data.scheduleList;
        this.mySchedule = data.mySchedule;
      }
    );
  }

  signIn(scheduleId): void {
    this.scheduleService.signIn(scheduleId).subscribe(
      data => {
        this.mySchedule = data.mySchedule;
        if (data.result) {
          this.scheduleList.forEach(schedule => {
            if (schedule.id === scheduleId) {
              schedule.participants++;
            }
          });
        }
      }
    );
  }

  signOut(scheduleId): void {
    this.scheduleService.signOut(scheduleId).subscribe(
      data => {
        this.mySchedule = data.mySchedule;
        if (data.result) {
          this.scheduleList.forEach(schedule => {
            if (schedule.id === scheduleId) {
              schedule.participants--;
            }
          });
        }
      }
    );
  }
}
