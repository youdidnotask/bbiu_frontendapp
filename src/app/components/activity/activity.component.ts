import { Component, OnInit } from '@angular/core';
import {UserService} from '../../service/user.service';
import {ScheduleService} from '../../service/schedule.service';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {
  roomList;
  activityList;
  trainerList;
  scheduleForm;
  successMsg;
  errorMsg;

  constructor(
    public userService: UserService,
    private scheduleService: ScheduleService,
    private formBuilder: FormBuilder
  ) {
    this.scheduleForm = this.formBuilder.group({
      roomId: ['', Validators.required],
      activityId : ['', Validators.required],
      trainerId: ['', Validators.required],
      day: ['', Validators.required],
      time: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.scheduleService.getRooms().subscribe(
      data => {
        this.roomList = data;
      }
    );
    this.scheduleService.getActivities().subscribe(
      data => {
        this.activityList = data;
      }
    );
    this.scheduleForm.get('activityId').valueChanges.subscribe(
      selectedActivity => {
        this.activityList.forEach(activity => {
          if (selectedActivity === activity.id) {
            this.trainerList = activity.trainers;
          }
        });
      }
    );
  }

  save(form): void {
    this.successMsg = '';
    this.errorMsg = '';
    this.scheduleService.save(form).subscribe(
      data => {
        this.successMsg = 'Activity successfully added';
        this.scheduleForm.reset();
      }, response => {
        this.errorMsg = response.error;
      }
    );
  }
}
