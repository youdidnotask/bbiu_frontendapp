import { Component, OnInit } from '@angular/core';
import { UserService } from './service/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    public userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {}

  public logout() {
    this.userService.signOut();
    this.router.navigateByUrl('/');
  }
}
